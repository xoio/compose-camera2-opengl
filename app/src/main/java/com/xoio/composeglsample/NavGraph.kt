package com.xoio.composeglsample

import android.graphics.Bitmap
import android.hardware.camera2.CameraManager
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.compose.NavHost
import androidx.navigation.NavHostController
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.xoio.composeglsample.screens.CameraScreen

@Composable
fun NavGraph(
    cameraManager: CameraManager,
    photos:MutableList<Bitmap>,
    navController: NavHostController = rememberNavController(),
    startDestination: String = "home"
) {

    NavHost(
        navController = navController,
        startDestination = startDestination,
        modifier = Modifier
    ) {

        composable("home") {
            CameraScreen(cameraManager)
        }
    }
}
