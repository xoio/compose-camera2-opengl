package com.xoio.composeglsample.gl

import android.graphics.Bitmap
import android.opengl.GLES30
import android.opengl.GLSurfaceView
import android.opengl.GLUtils
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10

/**
 * Renderer for the GLSurfaceView.
 */
class CameraRenderer : GLSurfaceView.Renderer {

    private lateinit var renderPlane: RenderPlane

    private var cameraImage = intArrayOf(0)
    private var setupDone = false
    private var currentFrame: Bitmap? = null


    override fun onSurfaceCreated(unused: GL10, config: EGLConfig) {
        // Set the background frame color
        GLES30.glClearColor(0.0f, 0.0f, 0.0f, 1.0f)

        GLES30.glGenTextures(1, cameraImage, 0)

        // setup format
        GLES30.glBindTexture(GLES30.GL_TEXTURE_2D, cameraImage[0]);

        // Set filtering
        GLES30.glTexParameteri(
            GLES30.GL_TEXTURE_2D,
            GLES30.GL_TEXTURE_MIN_FILTER,
            GLES30.GL_NEAREST
        );
        GLES30.glTexParameteri(
            GLES30.GL_TEXTURE_2D,
            GLES30.GL_TEXTURE_MAG_FILTER,
            GLES30.GL_NEAREST
        );

        GLES30.glTexParameteri(
            GLES30.GL_TEXTURE_2D,
            GLES30.GL_TEXTURE_WRAP_R,
            GLES30.GL_CLAMP_TO_EDGE
        )
        GLES30.glTexParameteri(
            GLES30.GL_TEXTURE_2D,
            GLES30.GL_TEXTURE_WRAP_S,
            GLES30.GL_CLAMP_TO_EDGE
        )
        GLES30.glTexParameteri(
            GLES30.GL_TEXTURE_2D,
            GLES30.GL_TEXTURE_WRAP_T,
            GLES30.GL_CLAMP_TO_EDGE
        )

        renderPlane = RenderPlane()
        setupDone = true
    }

    fun updateSurface(img: Bitmap) {

        if (setupDone) {
            currentFrame = img
        }
    }

    override fun onDrawFrame(unused: GL10) {

        // Redraw background color
        GLES30.glClear(GLES30.GL_COLOR_BUFFER_BIT)

        if (setupDone && currentFrame != null) {

            GLUtils.texImage2D(GLES30.GL_TEXTURE_2D, 0, currentFrame, 0)

            render()
        }
    }

    fun render() {
        renderPlane.draw(cameraImage[0])
    }

    override fun onSurfaceChanged(unused: GL10, width: Int, height: Int) {
        GLES30.glViewport(0, 0, width, height)
    }
}
