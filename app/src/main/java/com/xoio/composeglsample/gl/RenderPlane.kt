package com.xoio.composeglsample.gl

import android.opengl.GLES30

/**
 * Renders a full screen triangle for rendering texture.
 */
class RenderPlane {

    private var textureLoc = 0

    // NOTE! String formatting can work on some GPUs and not on others, for example
    // \\\n will be fine on Nvidia GPUs but on Ardeno it will cause shader program to not link.
    private var vertex = "#version 300 es \n in vec2 position;\n" +
            "  out vec2 uv;\n" +
            "  void main() {\n" +
            "   uv = vec2((gl_VertexID << 1) & 2, gl_VertexID & 2);\n" +
            "   gl_Position = vec4(uv * 2.0f + -1.0f, 0.0f, 1.0f);" +
            "  }"


    private var fragment = "#version 300 es \n" +
            " precision highp float;\n" +
            "  in vec2 uv;\n" +
            "  uniform sampler2D uTex0;\n" +
            "  out vec4 glFragColor;\n" +
            "  void main() {\n" +
            "    vec4 data = texture(uTex0,vec2(uv.x, 1.0 - uv.y));" +
            "    glFragColor = vec4(1.0,1.0, 0.0, 1.0);\n" +
            "    glFragColor = data;\n" +
            "  }"

    // shader program.
    private var program = 0;

    init {
        // build shader
        val vertexShader = compileShader(GLES30.GL_VERTEX_SHADER, vertex)
        val fragmentShader = compileShader(GLES30.GL_FRAGMENT_SHADER, fragment)

        program = GLES30.glCreateProgram().also {
            GLES30.glAttachShader(it, vertexShader)
            GLES30.glAttachShader(it, fragmentShader)
            GLES30.glLinkProgram(it)

            // check for error
            checkProgramLink(it)
        }

        // get texture location
        GLES30.glUseProgram(program)
        textureLoc = GLES30.glGetUniformLocation(program, "uTex0")
    }

    /**
     * Renders the plane.
     */
    fun draw(img: Int) {
        // bind program
        GLES30.glUseProgram(program)
        GLES30.glBindTexture(GLES30.GL_TEXTURE_2D, img);

        GLES30.glUniform1i(textureLoc, 0);

        GLES30.glDrawArrays(GLES30.GL_TRIANGLES, 0, 3)

    }
}
