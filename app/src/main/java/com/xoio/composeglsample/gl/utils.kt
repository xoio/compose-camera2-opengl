package com.xoio.composeglsample.gl

import android.opengl.GLES30
import android.util.Log

/**
 * Compiles a shader source string into a shader object.
 *
 * @param type the type of shader, GLES30.GL_VERTEX_SHADER , etc..
 * @param source the source for the shader
 *
 * @return the compiled shader object if successful, -1 if not successful.
 */
fun compileShader(type: Int, source: String): Int {
    val shader = GLES30.glCreateShader(type).also { shader ->
        run {

            GLES30.glShaderSource(shader, source)
            GLES30.glCompileShader(shader)

            // get compile status
            var compiled = intArrayOf(0)
            GLES30.glGetShaderiv(shader, GLES30.GL_COMPILE_STATUS, compiled, 0)

            if (compiled[0] == 0) {
                val shaderType = if (type == GLES30.GL_VERTEX_SHADER) "VERTEX" else "FRAGMENT"

                Log.e(
                    "$shaderType SHADER COMPILE FAILED",
                    "Log is " + GLES30.glGetShaderInfoLog(shader)
                )
                return -1
            }

        }
    }

    return shader
}

/**
 * Checks to ensure that the Shader program has linked correctly.
 *
 * @param prog the reference to the shader program.
 *
 * @return returns true or false depending on whether or not any linking errors were present.
 */
fun checkProgramLink(prog: Int): Boolean {

    val link = intArrayOf(0)
    GLES30.glGetProgramiv(prog, GLES30.GL_LINK_STATUS, link, 0)

    if (link[0] <= 0) {
        Log.e("SHADER PROGRAM LINK FAILED", "Attempting to link the shader program failed.")
        throw java.lang.Error("SHADER PROGRAM LINK FAILED")
    }

    return true
}