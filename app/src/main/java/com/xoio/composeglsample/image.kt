package com.xoio.composeglsample

import android.graphics.Bitmap
import androidx.compose.foundation.Image
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.asImageBitmap


@Composable
fun ImageObject(img: Bitmap) {

    Image(bitmap = img.asImageBitmap(), contentDescription = "Test" )

}
