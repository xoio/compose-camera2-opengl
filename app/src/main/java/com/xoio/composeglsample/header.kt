package com.xoio.composeglsample

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import com.xoio.composeglsample.ui.theme.HEADING_SIZE
import com.xoio.composeglsample.ui.theme.PADDING
import com.xoio.composeglsample.ui.theme.Typography
import com.xoio.composeglsample.ui.theme.title_color


@Composable
fun Header() {
    AppTitle()

}

@Preview
@Composable
fun AppTitle() {

    Row(
        modifier = Modifier.fillMaxWidth(1f)
            .background(Color.Transparent)
            .height(IntrinsicSize.Min)
            .padding(PADDING)
            .heightIn(Dp(0.0f),Dp(120.0f))
    ) {

        Text(
            stringResource(id = R.string.app_name),
            fontSize = HEADING_SIZE,
            style= Typography.h1,
            color = title_color
        )
    }
}
