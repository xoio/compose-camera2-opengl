package com.xoio.composeglsample.screens

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.SurfaceTexture
import android.hardware.camera2.CameraManager
import android.opengl.GLSurfaceView
import android.util.Log
import android.view.LayoutInflater
import android.view.TextureView
import android.view.TextureView.SurfaceTextureListener
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.viewinterop.AndroidView
import androidx.core.content.ContextCompat
import com.xoio.composeglsample.CameraHandler
import com.xoio.composeglsample.R
import com.xoio.composeglsample.gl.CameraRenderer
import java.util.concurrent.Executor
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine


suspend fun Context.getCameraProvider(): ProcessCameraProvider = suspendCoroutine { continuation ->
    ProcessCameraProvider.getInstance(this).also { future ->
        future.addListener(
            {
                continuation.resume(future.get())
            },
            executor
        )
    }
}

val Context.executor: Executor
    get() = ContextCompat.getMainExecutor(this)


//https://cs.android.com/androidx/platform/frameworks/support/+/androidx-main:camera/integration-tests/coretestapp/src/main/java/androidx/camera/integration/core/OpenGLRenderer.java

@SuppressLint("RestrictedApi", "Recycle")
@Composable
fun CameraScreen(
    cameraManager: CameraManager
) {

    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        val TAG = "CAMERA_SCREEN"

        val cameraHandler = CameraHandler(cameraManager)
        val renderer = CameraRenderer()
        AndroidView(
            factory = { ctx ->

                val view = LayoutInflater.from(ctx).inflate(R.layout.camera_view, null, false)


                // setup GL surface view to render things to
                val surfaceView = view.findViewById<GLSurfaceView>(R.id.camera_view)

                surfaceView.setEGLContextClientVersion(3)
                surfaceView.preserveEGLContextOnPause = true
                surfaceView.setRenderer(renderer)
                surfaceView.renderMode = GLSurfaceView.RENDERMODE_CONTINUOUSLY

                // setup camera preview and handler
                val textureView = view.findViewById<TextureView>(R.id.texture_view)
                cameraHandler.setup(textureView)

                textureView.surfaceTextureListener = object : SurfaceTextureListener {
                    override fun onSurfaceTextureAvailable(p0: SurfaceTexture, p1: Int, p2: Int) {
                        Log.e(TAG, "Surface Tex available")
                        cameraHandler.connectCamera()
                    }

                    override fun onSurfaceTextureSizeChanged(p0: SurfaceTexture, p1: Int, p2: Int) {
                        Log.e(TAG, "Image size changed")
                    }

                    override fun onSurfaceTextureDestroyed(p0: SurfaceTexture): Boolean {
                        Log.e(TAG, "destroyed")
                        return true
                    }

                    override fun onSurfaceTextureUpdated(p0: SurfaceTexture) {

                        textureView.bitmap?.let {
                            renderer.updateSurface(it)
                        }

                    }

                }

                view
            }
        )

    }
}
