package com.xoio.composeglsample.screens

import android.graphics.Bitmap
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.xoio.composeglsample.ImageObject
import com.xoio.composeglsample.R

@Composable
fun MainScreen(photos:MutableList<Bitmap>) {

    if (photos.size > 0) {
        LazyColumn {
            photos.forEach { img ->
                item {
                    ImageObject(img = img)
                }
            }
        }
    } else{
        Column(
            modifier= Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = stringResource(id = R.string.no_images),
            )
        }
    }
}