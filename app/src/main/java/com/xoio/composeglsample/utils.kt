package com.xoio.composeglsample

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import java.io.File

/**
 * Checks to see if there are photos in the specified directory. If there are, load the images
 * as Bitmaps and return in an array.
 *
 * @param context the app context
 * @return A mutable list of Bitmap objects to load into the view.
 */
fun check_for_photos(context: Context): MutableList<Bitmap> {
    // check to see if the photos folder exists and create if not
    val photos_folder = File("${context.filesDir}/photos")

    if (!photos_folder.exists()) {
        photos_folder.mkdir()
    }

    // start to load photos
    val files = photos_folder.listFiles()

    val images = mutableListOf<Bitmap>()
    for (img in files!!) {
        images.add(BitmapFactory.decodeFile(img.absolutePath.toString()))
    }

    return images
}