package com.xoio.composeglsample

object ScreenNames {
    const val HOME_ROUTE = "home"
    const val CAMERA_ROUTE = "camera"
}