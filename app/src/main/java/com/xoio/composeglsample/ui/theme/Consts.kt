package com.xoio.composeglsample.ui.theme

import androidx.compose.ui.text.font.Font
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.sp
import com.xoio.composeglsample.R


val PADDING = Dp(20.0f)

val HEADING_SIZE = 16.sp
val FONT_FAMILY = Font(R.font.switzer_extrabold)