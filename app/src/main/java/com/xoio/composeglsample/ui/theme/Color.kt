package com.xoio.composeglsample.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)


// core colors
val bg_color = Color(39, 49, 58, 255)
val ft_color = Color(28, 36, 43, 255)
val title_color = Color(223, 223, 223, 200)