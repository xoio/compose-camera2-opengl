package com.xoio.composeglsample

import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.hardware.camera2.CameraManager
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.xoio.composeglsample.ui.theme.UselessCameraTheme


class MainActivity : ComponentActivity() {

    private var TAG = "USELESS_CAMERA"

    // list of photos to show on the main screen.
    private lateinit var photos: MutableList<Bitmap>

    // Whether or not the camera permissions were granted.
    private var cameraPermissionsGranted: Boolean = false

    private val requestPermissionLauncher = registerForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { isGranted ->
        if (isGranted) {
            Log.i(TAG, "permissions granted")
            cameraPermissionsGranted = true
        } else {
            Log.e(TAG, "permissions not granted")

        }
    }

    private lateinit var cameraManager: CameraManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestCamera()
        run()
    }

    private fun run() {
        if (cameraPermissionsGranted) {
            cameraManager = getSystemService(Context.CAMERA_SERVICE) as CameraManager
        } else {
            return
        }

        // Load photos
        photos = check_for_photos(applicationContext)

        setContent {
            UselessCameraTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background,
                ) {

                    Column(
                        modifier = Modifier.fillMaxHeight(2f)
                    ) {

                        Header()
                        Box(
                            modifier = Modifier
                                .weight(1f)
                                .background(Color.Red)
                                .fillMaxSize()
                        ) {
                            NavGraph(
                                cameraManager = cameraManager,
                                photos = photos
                            )
                        }
                        Footer()
                    }

                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (cameraPermissionsGranted) {
            run()
        }
    }


    private fun requestCamera() {
        when {
            ContextCompat.checkSelfPermission(
                this,
                android.Manifest.permission.CAMERA
            ) == PackageManager.PERMISSION_GRANTED -> {
                Log.i(TAG, "Permission previously granted")
                cameraPermissionsGranted = true
            }

            ActivityCompat.shouldShowRequestPermissionRationale(
                this,
                android.Manifest.permission.CAMERA
            ) -> Log.i(TAG, "Showing permissions dialog")

            else -> requestPermissionLauncher.launch(android.Manifest.permission.CAMERA)
        }


    }
}
