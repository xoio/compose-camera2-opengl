package com.xoio.composeglsample

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.Dp
import com.xoio.composeglsample.ui.theme.PADDING
import com.xoio.composeglsample.ui.theme.ft_color


@Composable
fun Footer() {
    Row(
        modifier = Modifier
            .fillMaxWidth(1f)
            .background(ft_color)
            .height(IntrinsicSize.Min)
            .heightIn(Dp(0.0f), Dp(200.0f))
            .padding(PADDING),
        verticalAlignment = Alignment.Bottom
    ) {


        Column(
            modifier=Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Button(
                modifier = Modifier.width(Dp(50f)).height(Dp(50f)),
                onClick = { /*TODO*/ }) {}
        }
    }
}