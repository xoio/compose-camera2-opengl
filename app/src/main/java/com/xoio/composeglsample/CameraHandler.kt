package com.xoio.composeglsample

import android.annotation.SuppressLint
import android.graphics.ImageFormat
import android.graphics.SurfaceTexture
import android.hardware.camera2.*
import android.media.ImageReader
import android.media.ImageReader.OnImageAvailableListener
import android.os.Handler
import android.os.HandlerThread
import android.util.Log
import android.view.Surface
import android.view.TextureView

// reference
// https://www.freecodecamp.org/news/android-camera2-api-take-photos-and-videos/
class CameraHandler(
    private val cameraManager: CameraManager
) : OnImageAvailableListener {

    private val TAG = "CAMERA_MANAGER"

    private val cameraIds: Array<String> = cameraManager.cameraIdList
    private var cameraId: String = ""
    private lateinit var cameraCharacteristics: CameraCharacteristics
    private lateinit var imageReader: ImageReader

    lateinit var backgroundHandlerThread: HandlerThread
    lateinit var backgroundHandler: Handler

    lateinit var textureView: TextureView

    private lateinit var captureRequestBuilder: CaptureRequest.Builder
    private lateinit var cameraCaptureSession: CameraCaptureSession

    private val captureStateCallback = object: CameraCaptureSession.StateCallback() {
        override fun onConfigured(session: CameraCaptureSession) {
            cameraCaptureSession = session
            Log.e(TAG, "Camera Configured")
            cameraCaptureSession.setRepeatingRequest(
                captureRequestBuilder.build(),
                null,
                backgroundHandler
            )
        }

        override fun onConfigureFailed(p0: CameraCaptureSession) {
            Log.e(TAG, "Capture configure failed")
        }

    }

    private val cameraStateCallback = object : CameraDevice.StateCallback() {
        override fun onOpened(cameraDevice: CameraDevice) {

            Log.e(TAG, "Camera Opened")
            val surfaceTexture: SurfaceTexture? = textureView.surfaceTexture
            val previewSurface: Surface = Surface(surfaceTexture)

            captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW)
            captureRequestBuilder.addTarget(previewSurface)


            cameraDevice.createCaptureSession(
                listOf(previewSurface, imageReader.surface),
                captureStateCallback,
                null
            )
        }

        override fun onDisconnected(p0: CameraDevice) {
            TODO("Not yet implemented")
        }

        override fun onError(p0: CameraDevice, error: Int) {
            val errorMsg = when (error) {
                ERROR_CAMERA_DEVICE -> "Fatal (device)"
                ERROR_CAMERA_DISABLED -> "Device policy"
                ERROR_CAMERA_IN_USE -> "Camera in use"
                ERROR_CAMERA_SERVICE -> "Fatal (service)"
                ERROR_MAX_CAMERAS_IN_USE -> "Maximum cameras in use"
                else -> "Unknown"
            }
            Log.e(TAG, "Issue getting camera state $errorMsg")
        }

    }

    /**
     * Sets up the [CameraHandler] and connects it to the TextureView associated with the Camera
     * @param texView The texture view to use for processing.
     */
    fun setup(texView: TextureView) {

        this.textureView = texView

        startBackgroundThread()
        setupCamera()



    }

    /**
     * Sets up the camera and fetches the back facing camera on a device. Note that this does not check
     * to ensure that a back camera is available.
     */
    private fun setupCamera() {
        // get camera id
        for (id in cameraIds) {

            val cameraCharacteristics = cameraManager.getCameraCharacteristics(id)

            // Make sure we are using the back camera
            if (cameraCharacteristics.get(CameraCharacteristics.LENS_FACING) == CameraCharacteristics.LENS_FACING_BACK) {

                this.cameraCharacteristics = cameraCharacteristics
                cameraId = id

                break
            }
        }

        // set preview size
        val previewSize =
            cameraCharacteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)!!
                .getOutputSizes(
                    ImageFormat.JPEG
                ).maxByOrNull { it.height * it.width }!!

        imageReader =
            ImageReader.newInstance(previewSize.width, previewSize.height, ImageFormat.JPEG, 1)

        imageReader.setOnImageAvailableListener(this, backgroundHandler)
    }

    /**
     * Starts the thread for handling the camera. Assumes camera permission has already been acquired.
     */
    @SuppressLint("MissingPermission")
    private fun startBackgroundThread() {

        backgroundHandlerThread = HandlerThread("CameraVideoThread")
        backgroundHandlerThread.start()
        backgroundHandler = Handler(backgroundHandlerThread.looper)

    }

    @SuppressLint("MissingPermission")
    fun connectCamera() {
        cameraManager.openCamera(cameraId, cameraStateCallback, backgroundHandler)
    }

    /**
     * Stops the thread for handling the camera.
     */
    private fun stopBackgroundThread() {
        backgroundHandlerThread.quitSafely()
        backgroundHandlerThread.join()
    }

    override fun onImageAvailable(reader: ImageReader) {

        Log.e(TAG, "Image availablee")
    }
}
