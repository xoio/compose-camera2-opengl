Jetpack Compose + Camera Preview + OpenGL
===

This is just a simple reference for how to integrate Jetpack Compose with the Camera2 api and
output the result. I was looking to use as many modern techniques as possible but weirdly enough, it seems like no one had tried it this way up until this point. 


Requirements & Assumptions
====
* OpenGL ES 3.0 min
* Uses Camera2 / Camera X api 
* Uses Navigation Compose package.

References
==
[This article](https://www.freecodecamp.org/news/android-camera2-api-take-photos-and-videos/) from Free Code Camp really helped me get up to speed with the camera api and the basics of how it works. 

